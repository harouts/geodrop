package com.example.android.newgeodrop;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * Created by Harout on 11/21/17.
 */

public class MessageBox extends AppCompatEditText {

    private Context _mContext;

    /* Must use this constructor in order for the layout files to instantiate the class properly */
    public MessageBox(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this._mContext = context;
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean onKeyPreIme (int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            ((MapsActivity) _mContext).resetUI();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

}
