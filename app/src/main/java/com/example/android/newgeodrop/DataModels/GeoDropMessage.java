package com.example.android.newgeodrop.DataModels;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Harout on 9/14/17.
 */

public class GeoDropMessage implements Serializable {
    public LatLng latLng;

    public Long timestamp;

    public String message;

    public String id;

    public String user;

    public GeoDropMessage() {
        this(null, null, null, null, null);
    }

    public GeoDropMessage(LatLng latLng, String message, Long timestamp, String user) {
        this(latLng, message, timestamp, null, user);
    }

    public GeoDropMessage(LatLng latLng, String message, Long timestamp, String id, String user) {
        this.latLng = latLng;
        this.message = message;
        this.timestamp = timestamp;
        this.id = id;
        this.user = user;
    }


}
