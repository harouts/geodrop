package com.example.android.newgeodrop;

        import android.Manifest;
        import android.content.Context;
        import android.content.pm.PackageManager;
        import android.graphics.Color;
        import android.location.Location;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.app.FragmentActivity;
        import android.os.Bundle;
        import android.support.v4.content.ContextCompat;
        import android.util.Log;
        import android.view.Gravity;
        import android.view.KeyEvent;
        import android.view.View;
        import android.view.inputmethod.EditorInfo;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.Button;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.example.android.newgeodrop.DataModels.GeoDropMessage;
        import com.google.android.gms.location.FusedLocationProviderClient;
        import com.google.android.gms.location.LocationServices;
        import com.google.android.gms.maps.CameraUpdateFactory;
        import com.google.android.gms.maps.GoogleMap;
        import com.google.android.gms.maps.OnMapReadyCallback;
        import com.google.android.gms.maps.SupportMapFragment;
        import com.google.android.gms.maps.model.BitmapDescriptorFactory;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.android.gms.maps.model.Marker;
        import com.google.android.gms.maps.model.MarkerOptions;
        import com.google.android.gms.tasks.OnCompleteListener;
        import com.google.android.gms.tasks.OnSuccessListener;
        import com.google.android.gms.tasks.Task;
        import com.google.firebase.auth.AuthResult;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.auth.FirebaseUser;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.ValueEventListener;

        import java.sql.Timestamp;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap map;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 0;
    private FusedLocationProviderClient mFusedLocationClient;
    private Button leaveMessageButton;
    private Button dropButton;
    private Button deleteButton;
    private MessageBox messageBox;
    private float zoomLevel = 18;
    private InputMethodManager imm;
    private Location currentLocation;
    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private DatabaseReference gdmRef;
    //    private DatabaseReference chatsRef;
    private DatabaseReference users;
    private DatabaseReference curUser;
    private DatabaseReference testrootRef;
    private DatabaseReference testgdmRef;
    //    private DatabaseReference testchatsRef;
    private DatabaseReference testusers;
    private DatabaseReference testcurUser;
    private String userID;
    private Marker currentMarker;
    private boolean isInCameraRange;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

//will push entire activity up to make room for keyboard
//        MapsActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        testrootRef = database.getReference("testroot");
        testusers = testrootRef.child("testusers");
        testgdmRef = testrootRef.child("testgeodropMessages");
//        chatsRef = testrootRef.child("chats");
//        LatLng mainStarbucks = new LatLng(47.609325, -122.142352);
//        LatLng sixSix = new LatLng(47.615409, -122.202302);
//        LatLng myHouse = new LatLng(47.623970, -122.121248);
//        GeoDropMessage m1 = new GeoDropMessage(mainStarbucks, "this is main starbucks");
//        GeoDropMessage m2 = new GeoDropMessage(sixSix, "this is sixsix");
//        GeoDropMessage m3 = new GeoDropMessage(myHouse, "this is my house");
//        messagesRef.push().setValue(m1);
//        messagesRef.push().setValue(m2);
//        messagesRef.push().setValue(m3);
    }



    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = auth.getCurrentUser();

        auth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SIGNIN", "signInAnonymously:success");
                            FirebaseUser user = auth.getCurrentUser();
//                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("SIGNIN", "signInAnonymously:failure", task.getException());
                            Toast.makeText(MapsActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
        userID = currentUser.getUid();

        testcurUser = testusers.child(userID);
    }

    @Override
    public void onStop() {
        super.onStop();

        View view = findViewById(android.R.id.content).getRootView();
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        View view = findViewById(android.R.id.content).getRootView();
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        View view = findViewById(android.R.id.content).getRootView();
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        initialize();

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latlng) {
//                clickedLocation = latlng;
//                currentMarker = null;
                resetUI();
                deleteButton.setVisibility(View.INVISIBLE);

                //code for future click to drop message update
//                double userLat = currentLocation.getLatitude();
//                double userLong = currentLocation.getLongitude();
//
//                if ((distance(userLat, userLong, latlng.latitude, latlng.longitude) < 0.1))
//                {
//                    leaveMessageButton.setVisibility(View.VISIBLE);
//                }
            }

        });

        map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                resetUI();
                LatLng curLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                if (map.getProjection().getVisibleRegion().latLngBounds.contains(curLatLng)) {
                    isInCameraRange = true;
                    leaveMessageButton.setVisibility(View.VISIBLE);
                } else {
                    isInCameraRange = false;
                    leaveMessageButton.setVisibility(View.INVISIBLE);
                }
            }
        });



        leaveMessageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                resetUI();
                messageBox.setVisibility(View.VISIBLE);
                dropButton.setVisibility(View.VISIBLE);
                messageBox.requestFocus();
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);

                leaveMessageButton.setVisibility(View.INVISIBLE);

            }
        });



        dropButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setUserLocation();
                String userMessage = messageBox.getText().toString();
                LatLng userLatLng = new LatLng(currentLocation.getLatitude(),
                        currentLocation.getLongitude());

                Long timestamp = new Timestamp(System.currentTimeMillis()).getTime();
                GeoDropMessage message = new GeoDropMessage(userLatLng, userMessage, timestamp,null, userID);

                pushToServer(message);

                resetUI();

            }
        });

        messageBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_FLAG_NAVIGATE_PREVIOUS) {
                    resetUI();
                    handled = true;
                }
                return handled;
            }
        });

        testgdmRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                map.clear();

                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    String message = child.child("message").getValue().toString();
                    Double lat = (double) child.child("latLng").child("latitude").getValue();
                    Double longi = (double) child.child("latLng").child("longitude").getValue();
                    Long timestamp = (Long) child.child("timestamp").getValue();
                    String id = child.child("id").getValue().toString();
                    String userId = child.child("user").getValue().toString();

                    LatLng latLng = new LatLng(lat, longi);

                    Log.d("FROMDBID", id);

                    GeoDropMessage gdm = new GeoDropMessage(latLng, message, timestamp, id, userId);
                    putMessageOnMap(gdm);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker) {
                currentMarker = marker;

                resetUI();

                String[] ids = currentMarker.getTag().toString().split(":");


                final String messageId = ids[0];
                final String userId = ids[1];

                Log.d("userId", userId);
                Log.d("messageId", messageId);

                double markerLat = currentMarker.getPosition().latitude;
                double markerLong = currentMarker.getPosition().longitude;

                double userLat = currentLocation.getLatitude();
                double userLong = currentLocation.getLongitude();

                if ((distance(userLat, userLong, markerLat, markerLong) > 0.2))
                {
                    currentMarker.hideInfoWindow();
                    return true;
                }
                else {
                    if (userID.equals(userId)) {
                        Log.d("MARKER", currentMarker.getTag().toString());

                        deleteButton.setVisibility(View.VISIBLE);
//                        Log.d("USERID", userID + ":" + userId);

                        deleteButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
//                                Log.d("TAG", gdmId);
                                currentMarker.remove();
                                deleteButton.setVisibility(View.INVISIBLE);
                                testgdmRef.child(messageId).removeValue();
                                testcurUser.child(messageId).removeValue();


                            }
                        });
                    } else {
                        deleteButton.setVisibility(View.INVISIBLE);
                    }
                    //toast that shows id of marker aka id of gdm associated with it
//                Context context = getApplicationContext();
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, (CharSequence) marker.getTag(), duration);
//                toast.show();
                    return false;
                }

            }
        });




    }

    public void initialize() {
        isInCameraRange = true;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        leaveMessageButton = (Button) findViewById(R.id.message_button);
        dropButton = (Button) findViewById(R.id.drop_button);
        deleteButton = (Button) findViewById(R.id.delete_button);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        messageBox = (MessageBox) findViewById(R.id.message_input);
        messageBox.setBackgroundColor(Color.WHITE);
        messageBox.setGravity(Gravity.TOP);
        setUserLocation();

        map.moveCamera(CameraUpdateFactory.zoomTo(zoomLevel));




    }

    @Override
    public void onBackPressed() {
        resetUI();
        super.onBackPressed();
    }

    //latLng coming back null from database
    public void putMessageOnMap(GeoDropMessage message) {
        Log.d("messageContents", message.message.toString());
        Marker mark = map.addMarker(new MarkerOptions().position(message.latLng)
                .title(message.message).icon(BitmapDescriptorFactory
                        .fromResource(R.mipmap.message_icon)));
        mark.setTag(message.id + ":" + message.user);

//        Context context = getApplicationContext();
//        int duration = Toast.LENGTH_SHORT;
//
//        Toast toast = Toast.makeText(context, (CharSequence) mark.getTag(), duration);
//        toast.show();

    }

    public void setUserLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            map.setMyLocationEnabled(true);

            mFusedLocationClient.getLastLocation().addOnSuccessListener(this,
                    new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {

                                //needed to set camera
                                double userLat = location.getLatitude();
                                double userLng = location.getLongitude();
                                LatLng current = new LatLng(userLat, userLng);

                                //keep track of location globally
                                currentLocation = location;

                                map.moveCamera(CameraUpdateFactory.newLatLng(current));

                                //code for future click to drop message update
//                        radiusRing = map.addCircle(new CircleOptions()
//                                .center(current)
//                                .radius(50)
//                                .strokeColor(Color.BLUE)
//                                .fillColor(Color.TRANSPARENT));
//
//                        radiusRing.setCenter(current);
                            }
                        }
                    });

        }
    }

    /** calculates the distance between two locations in MILES */
    private double distance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 3958.75; // in miles, change to 6371 for kilometer output

        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        double dist = earthRadius * c;

        return dist; // output distance, in MILES
    }

    public void pushToServer(GeoDropMessage message) {
        String gdmID = testgdmRef.push().getKey();

        message.id = gdmID;

        testgdmRef.child(gdmID).setValue(message);

//        chatsRef.child(gdmID);

        testcurUser.child(gdmID).setValue(message);

//        putMessageOnMap(message);

    }

    public void resetUI() {
        if (isInCameraRange) {
            leaveMessageButton.setVisibility((View.VISIBLE));
        } else {
            deleteButton.setVisibility(View.INVISIBLE);
        }

        messageBox.setText("");
        messageBox.setVisibility(View.INVISIBLE);
        dropButton.setVisibility(View.INVISIBLE);

        View view = findViewById(android.R.id.content).getRootView();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d("PERMISSION", "permission was granted");
                    setUserLocation();



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.d("PERMISSION", "permission was not granted");
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
